import { callApi, GetFighterByIdType } from '../helpers/apiHelper';
import { IFighter } from '../helpers/mockData';

class FighterService {
  async getFighters(): Promise<IFighter[]> {
    try {
      const endpoint = 'fighters.json';
      const apiResult = await callApi(endpoint, 'GET');

      return apiResult as IFighter[];
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id: string): Promise<GetFighterByIdType> {
    try {
      const endpoint = `details/fighter/${id}.json`;
      const apiResult = await callApi(endpoint, 'GET');
     
      return apiResult as GetFighterByIdType;
    } catch (error) {
      throw error;
    }
  }
}

export const fighterService = new FighterService();
