import { controls } from '../../constants/controls';
import { decreaseHealth, decreaseHealthIndicator, getDamage, state, getCriticalHitPower, callback } from './fight';
import { IFighterDetails } from '../helpers/mockData';

const {PlayerOneAttack, PlayerOneBlock, PlayerTwoAttack, PlayerTwoBlock, PlayerOneCriticalHitCombination, PlayerTwoCriticalHitCombination} = controls;
const TEN_SEC = 10000;

type ResolveType = (value: IFighterDetails) => void;

export function keyDownHandler(
  firstFighter: IFighterDetails,
  secondFighter: IFighterDetails,
  resolve: ResolveType,
  event: KeyboardEvent
) {
  const key = event.code;

  switch (key) {
    case PlayerOneAttack:  
      if (state.isFirstFighterBlockActivated) return;
      decreaseHealth(true, getDamage(firstFighter, secondFighter));
      decreaseHealthIndicator(true);
      break;    
    case PlayerTwoAttack:  
      if (state.isSecondFighterBlockActivated) return;
      decreaseHealth(false, getDamage(secondFighter, firstFighter));
      decreaseHealthIndicator(false);
      break;
    case PlayerOneBlock:
      state.isFirstFighterBlockActivated = true; 
      break;
    case PlayerTwoBlock:      
      state.isSecondFighterBlockActivated = true;
      break;
  }

  if (PlayerOneCriticalHitCombination.includes(key) && !state.firstFighterKeysArray.includes(key)) {
    state.firstFighterKeysArray.push(key)
  } else if (PlayerTwoCriticalHitCombination.includes(key) && !state.secondFighterKeysArray.includes(key)) {
    state.secondFighterKeysArray.push(key)
  }

  if (state.firstFighterKeysArray.length === 3 && state.timeFirstFighterCriticalHit! + TEN_SEC < Date.now()) {
    decreaseHealth(true, getCriticalHitPower(firstFighter));
    decreaseHealthIndicator(true);
    state.timeFirstFighterCriticalHit = Date.now();
  } else if (state.secondFighterKeysArray.length === 3 && state.timeSecondFighterCriticalHit! + TEN_SEC < Date.now()) {
    decreaseHealth(false, getCriticalHitPower(secondFighter));
    decreaseHealthIndicator(false);
    state.timeSecondFighterCriticalHit = Date.now();
  }

  if (state.firstFighterCurrentHealth! <= 0) {
    document.removeEventListener('keydown', callback);
    document.removeEventListener('keyup', keyUpHandler);
    resolve(secondFighter)
  } else if (state.secondFighterCurrentHealth! <= 0) {
    document.removeEventListener('keydown', callback);
    document.removeEventListener('keyup', keyUpHandler);
    resolve(firstFighter)
  }
}

export function keyUpHandler(event: KeyboardEvent) {
  const key = event.code;

  switch (key) {
    case PlayerOneBlock:
      state.isFirstFighterBlockActivated = false;  
      break; 
    case PlayerTwoBlock:      
      state.isSecondFighterBlockActivated = false;
      break;
  }

  if (state.firstFighterKeysArray.includes(key)) {
    const pos = state.firstFighterKeysArray.indexOf(key);
    state.firstFighterKeysArray.splice(pos, 1);
  } else if (state.secondFighterKeysArray.includes(key)) {
    const pos = state.secondFighterKeysArray.indexOf(key);
    state.secondFighterKeysArray.splice(pos, 1);
  }
}