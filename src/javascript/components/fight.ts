import { keyDownHandler, keyUpHandler } from './eventHandlers';
import { IFighterDetails } from '../helpers/mockData';

interface IState {
  firstFighter: IFighterDetails | {}
  secondFighter: IFighterDetails | {}
  firstFighterCurrentHealth: number | null
  secondFighterCurrentHealth: number | null
  initialWidthHealthIndicator: number | null
  isFirstFighterBlockActivated: boolean
  isSecondFighterBlockActivated: boolean
  timeFirstFighterCriticalHit: number | null
  timeSecondFighterCriticalHit: number | null
  firstFighterKeysArray: string[]
  secondFighterKeysArray: string[]
}

export const state: IState = {
  firstFighter: {},
  secondFighter: {},
  firstFighterCurrentHealth: null,
  secondFighterCurrentHealth: null,
  initialWidthHealthIndicator: null,
  isFirstFighterBlockActivated: false,
  isSecondFighterBlockActivated: false,
  timeFirstFighterCriticalHit: null,
  timeSecondFighterCriticalHit: null,
  firstFighterKeysArray: [],
  secondFighterKeysArray: []
}

export let callback: (event: KeyboardEvent) => void;

export async function fight(
  firstFighter: IFighterDetails,
  secondFighter: IFighterDetails
): Promise<IFighterDetails> {
  setInitialState(firstFighter, secondFighter);

  return new Promise((resolve) => {
    callback = keyDownHandler.bind(null, firstFighter, secondFighter, resolve);
    document.addEventListener('keydown', callback);
    document.addEventListener('keyup', keyUpHandler);
  })
}

export function getDamage(attacker: IFighterDetails, defender: IFighterDetails) {
  if (attacker === state.firstFighter && state.isSecondFighterBlockActivated) return 0;
  if (attacker === state.secondFighter && state.isFirstFighterBlockActivated) return 0;
  const damage = getHitPower(attacker) - getBlockPower(defender); 
  return Math.max(0, damage)
}

export function getHitPower(fighter: IFighterDetails) {
  const hitPower = fighter.attack * (Math.random() + 1);
  return hitPower
}

export function getBlockPower(fighter: IFighterDetails) {
  const blockPower = fighter.defense * (Math.random() + 1);
  return blockPower
}

export function getCriticalHitPower(fighter: IFighterDetails) {
  const criticalHitPower = fighter.attack * 2
  return criticalHitPower
}

export function decreaseHealth(isFirstFighterAttack: boolean, damage: number) {
  const currentHealth = isFirstFighterAttack ? 'secondFighterCurrentHealth' : 'firstFighterCurrentHealth'
  state[currentHealth]! -= damage;
}

export function decreaseHealthIndicator(isFirstFighterAttack: boolean) {
  const defender = isFirstFighterAttack ? state.secondFighter as IFighterDetails : state.firstFighter as IFighterDetails;
  const currentHealth = isFirstFighterAttack ? state.secondFighterCurrentHealth : state.firstFighterCurrentHealth;
  const position = isFirstFighterAttack ? 'right' : 'left';

  const healthIndicator = document.getElementById(`${position}-fighter-indicator`) as HTMLElement;

  const newWidth = state.initialWidthHealthIndicator! * currentHealth! / defender.health;
  healthIndicator.style.width = newWidth > 0 ? newWidth + 'px' : '0';
}

function setInitialState(firstFighter: IFighterDetails, secondFighter: IFighterDetails) {
  state.firstFighter = firstFighter;
  state.secondFighter = secondFighter;
  state.firstFighterCurrentHealth = firstFighter.health;
  state.secondFighterCurrentHealth = secondFighter.health;

  const healthIndicator = document.getElementById(`left-fighter-indicator`) as HTMLElement;
  state.initialWidthHealthIndicator = healthIndicator.clientWidth;
}





