import { showModal } from './modal';
import { IFighter } from '../../helpers/mockData';

export function showWinnerModal(fighter: IFighter) {
  showModal({
    title: `${fighter.name} won!!!`,
    bodyElement: ''
  })
}
