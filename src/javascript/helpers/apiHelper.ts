import { fightersDetails, fighters, IFighterDetails, IFighter } from './mockData';

const API_URL = 'https://api.github.com/repos/binary-studio-academy/stage-2-es6-for-everyone/contents/resources/api/';
const useMockAPI = true;

type CallApiType = IFighter[] | GetFighterByIdType;

async function callApi(endpoint: string, method: string): Promise<CallApiType> {
  const url = API_URL + endpoint;
  const options = {
    method
  };

  return useMockAPI
    ? fakeCallApi(endpoint)
    : fetch(url, options)
        .then((response) => (response.ok ? response.json() : Promise.reject(Error('Failed to load'))))
        .then((result) => JSON.parse(atob(result.content)))
        .catch((error) => {
          throw error;
        });
}

async function fakeCallApi(endpoint: string): Promise<CallApiType> {
  const response = endpoint === 'fighters.json' ? fighters : getFighterById(endpoint);

  return new Promise<CallApiType>((resolve, reject) => {
    setTimeout(() => (response ? resolve(response) : reject(Error('Failed to load'))), 500);
  });
}

export type GetFighterByIdType = IFighterDetails | undefined;

function getFighterById(endpoint: string): GetFighterByIdType {
  const start = endpoint.lastIndexOf('/');
  const end = endpoint.lastIndexOf('.json');
  const id = endpoint.substring(start + 1, end);

  return fightersDetails.find((it) => it._id === id);
}

export { callApi };
