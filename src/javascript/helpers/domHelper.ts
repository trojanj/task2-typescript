interface ICreateElementArgs {
  tagName: string
  className?: string
  attributes?: Record<string, string>
}

export function createElement({ tagName, className, attributes = {} }: ICreateElementArgs): HTMLElement {
  const element = document.createElement(tagName);

  if (className) {
    const classNames = className.split(' ').filter(Boolean);
    element.classList.add(...classNames);
  }

  Object.keys(attributes).forEach((key) => element.setAttribute(key, attributes[key]));

  return element;
}
