interface IControls {
  PlayerOneAttack: string
  PlayerOneBlock: string
  PlayerTwoAttack: string
  PlayerTwoBlock: string
  PlayerOneCriticalHitCombination: [string, string, string]
  PlayerTwoCriticalHitCombination: [string, string, string]
}

export const controls: IControls = {
  PlayerOneAttack: 'KeyA',
  PlayerOneBlock: 'KeyD',
  PlayerTwoAttack: 'KeyJ',
  PlayerTwoBlock: 'KeyL',
  PlayerOneCriticalHitCombination: ['KeyQ', 'KeyW', 'KeyE'],
  PlayerTwoCriticalHitCombination: ['KeyU', 'KeyI', 'KeyO']
}